package server

import (
	"net/http"

	"gitlab.com/srae/xsdserver/config"
)

// Server : wrapper for the server's configuration and router
// (this is to avoid evil global variables)
type Server struct {
	Conf   *config.Config
	Router *http.ServeMux
}

// NewServer : Server constructor. Init the router & returns a ptr to the Server struct.
func NewServer(cfg *config.Config) *Server {
	server := &Server{
		Conf:   cfg,
		Router: http.NewServeMux(),
	}
	server.routes()
	return server
}

// ServerHTTP : Making "Server" an implementation of http.Handle.
// It can then be passed to http.ListenAndServe.
func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.Router.ServeHTTP(w, r)
}

// GetPort : port acessor (abstraction of the conf)
func (s *Server) GetPort() string {
	return s.Conf.Port
}
