package server

func (s *Server) routes() {
	s.Router.HandleFunc("/", handleIndex)
	s.Router.HandleFunc("/health", handleHealth)
	s.Router.HandleFunc("/validation", s.handleValidation())
}
