package server

import (
	"net/http"
)

func (s *Server) handleValidation() http.HandlerFunc {
	var (
		err error
	)

	return func(w http.ResponseWriter, r *http.Request) {
		// Loading the xsd only the first time the validation endpoint is actually called.
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("500 - Could not load xsd schema."))
			return
		}

		switch r.Method {
		case http.MethodPost:
			w.WriteHeader(http.StatusNotImplemented)
			w.Write([]byte("501 - Not implemented (the validator isn't ready yet)"))
			// TODO : Implement xsd validation here.
		default:
			w.WriteHeader(http.StatusMethodNotAllowed)
			w.Write([]byte("405 - Method not allowed"))
		}
	}
}
