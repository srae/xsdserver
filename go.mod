module gitlab.com/srae/xsdserver

go 1.13

require (
	github.com/lestrrat-go/libxml2 v0.0.0-20191008001243-8ad9bf187c39
	github.com/pkg/errors v0.8.1
	gopkg.in/yaml.v2 v2.2.7
)
