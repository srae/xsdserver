package config

import (
	"io/ioutil"

	"github.com/lestrrat-go/libxml2/xsd"
	"gopkg.in/yaml.v2"
)

// Config : The server configuration
type Config struct {
	Port string
}

// LoadConfig : load the yml config & parse it as a Config struct.
func LoadConfig(fileName string) (*Config, error) {
	source, err := ioutil.ReadFile(fileName)
	if err != nil {
		return nil, err
	}
	config := &Config{}
	err = yaml.Unmarshal(source, config)
	if err != nil {
		return nil, err
	}
	return config, nil
}

// LoadSchema : loads the xsd schema file & parse it as a xsd.Schema struct.
func LoadSchema(fileName string) (*xsd.Schema, error) {
	schemaFile, err := xsd.ParseFromFile(fileName)
	if err != nil {
		return nil, err
	}
	return schemaFile, nil
}
